+++

title = "NFTs as a Contemporary Disruptor"
comments = "false"
+++


Satoshi Nakamoto (Nuk-om-oto Sat-o-shi) is the name used by a presumed
pseudonymous (Soo-daa-nah-mus)  person or persons, who invented bitcoin and has
a $60 billion fortune to their name. Nobody knows who they actually are.

Blockchain is a technology developed specifically for BitCoin, it is a data
structure that makes it possible to make a digital archive of data that can be
shared among individuals. At present there are four types of BlockChain. Public,
Private, Hybrid and Consortium.

Public Blockchain is where cryptocurrencies such as BitCoin originated from.
This BlockChain is Non-restrictive and permissionless, anyone with access to the
internet can login in and join the network. Once joined any user can access past
and current records, no valid record or transaction can be changed and anyone
can find bugs or propose changes as the source code is usually an open source.

Private Blockchain is a network that is run like a closed network, and is under
the control of one person/persons. While it works much like the public
Blockchain, in the same way that it uses peer to peer connections and
decentralisation, it’s usually on a much smaller scale, such as within a company
or organisation.

Hybrid Blockchain is a type of technology that uses parts of both the public and
private technologies. It allows a public Blockchain to be run alongside a
private one, allowing the organizers to decide who can access what and what
parts can be accessed by the public. 

Consortium BlockChain is also known as a federated BlockChain, it is similar to
the Hybrid BlockChain as it combines both public and private, but different in
that it eliminates the risks that come with one entity controlling the network
on a private BlockChain.

BitCoin is basically Bank free digital money. It is classified as decentralized
money, meaning there is no bank or central authority governing bitcoin. It is
controlled by the network of users who control and verify the monetary
transactions

Bitcoin was the first blockchain technology, which was specifically made for the
use of cryptocurrency. Unlike Bitcoin Ethereum was not created just for
cryptocurrency, rather it was designed to be different, it’s core features being
the smart contract and Ether. 

Ether is the primary cryptocurrency supported by Ethereum, although you can
create your own exchangeable tokens. Smart contracts provide an execution
environment that ensures integrity across all nodes. Basically if a change is
made on one node, then the same change is made on all nodes.

This guarantees that a whole lot of exchanges can happen without worry for
fraud, censorship, or any third party involvement. Ethereum does not require a
bank, broker wholesaler to provide trust. It allows transactions faster, with
far lower service fees or the need for approval from external authorities.

NFT or a Non-Fungible Token is a unique identifier that can cryptographically
assign and prove ownership of digital goods such as art, video clips, music and
real estate. Basically they are a digital proof of ownership and identity. It
uses blockchain technology to ensure that the product is the original, this
makes it very difficult to alter or counterfeit NFT’S.  Platforms such as
OpenSea are marketplaces for NFTs that operate on Ethereum. It allows users to
interact with the network to exchange NFTs for cryptocurrency

NFTs have been used to sell GIFs, photography, memes, tweets, video games,
music, digital art and more. They are technically a type of cryptocurrency,
however, one of the things that set NFTs apart from let’s say, Bitcoin tokens,
is that they are each unique and cannot be exchanged like-for-like. This means
that just like the art that you find hanging in a gallery, each NFT is valued
individually based on changing market demand.

Many artists across different mediums are stepping into the NFT space,
identifying an alternative and perhaps more lucrative opportunity to sell their
art. Artists, gamers, musicians, filmmakers and even brands are finding success
in what is becoming a growing market with a lot of money to be made. 

One of the fastest growing trends currently taking place is the way that in-game
purchases in video games are shifting. As consumer confidence and popularity
continues to grow, entire games are now being developed around NFTs, with the
process of buying them allowing the buyer to own a specific piece of the game
and to continue to buy and sell them, adding extra value along the way based on
who the previous owner was.

One of the most well-known success stories to date comes from the sale of CGI
expert Mike Winklemann, selling the first ever NFT through the prestigious
Christies Auction House. The collage collection of his work titled ‘Beeple’s
Everydays – The First 5000 Days’, sold for a staggering US$69million and remains
the most expensive sale for an NFT and one of the most expensive artworks ever
sold in general. 

Another notable sale was by Twitter CEO, Jack Dorsey, who sold his first ever
tweet as an NFT for almost US$3 million. Recently, Digital Artist Krista Kim
created and sold the first digital home ‘Mars House: The first digital NFT home
in the world’, for US$500,000. 

NFTs have also allowed an opportunity for musicians to access another revenue
stream by selling the rights and originals of their work, as well as music clips
they create.

NFTs are certainly an economic mechanism, with the potential to disrupt the
current rules of ownership across different markets. Transactions of ownership
usually depend on several middlemen, however transactions that are recorded on
blockchains, like NFTs, are reliable because the information cannot be altered.
This opens up the opportunity for more transparency in transactions and further
opportunities that are yet to be imagined.

As with any relatively new technology or economic phenomenon, the trading of
NFTs raises a number of legal and ethical problems which are complex and
difficult to unpack. Chief among these is the question of value, and
specifically, what commodity is actually being traded in the exchange.

The term non-fungible, in economic terms, refers to an asset which cannot be
traded for another asset of identical value. In this sense, a physical artwork
or real-world artifact is a non-fungible asset–the artwork can be traded for
another of equal monetary value, but this exchange is not a transaction of
identical value. When a patron buys a physical work of art, they are purchasing
the real-world product of the artist’s labour and expertise; this product cannot
be reproduced as an exact copy–even a highly skilled forgery is uniquely
different from the original artist’s work.

In contrast, money is the archetypal fungible asset; one dollar can be exchanged
for another dollar, and at any point in time, both dollars are considered to
hold identical value. Likewise, 20 dollars in any denomination of coins can be
exchanged for a single $20 note and still hold identical value.

Digital artworks can be reproduced with ease, and each copy is identical in
every respect to the original. Moreover, as is the case with the sale of GIFs
and memes, the product itself may have already been widely distributed on the
Internet, and those copies made freely available to any and all who download
them. Nothing highlights this point more clearly than the recent sale of the
original source code for the Internet, produced by Tim Berners-Lee, which sold
for 5.4 million US dollars in July this year (at a public auction which took
place, quite poignantly, on the Internet).

This raises the question of what, exactly, does an NFT represent? And how do you
define the value of an asset which anyone with access to the Internet can obtain
for free? Firstly, the sale of an NFT does not typically bestow the purchaser
with copyright of the original work; as with traditional art sales, you are
only purchasing ownership of only one copy of the artwork. One of the economic
factors which defines the value of a physical work is the concept of
‘scarcity’. You can purchase a high-quality poster or print of the Mona Lisa
for less than $50, but there exists only one version of the original, which
the Louvre in Paris has insured for $100 million. NFTs are an attempt to
introduce scarcity to digital assets; there may be millions of identical
copies of a digital asset available online, but there is (and only ever can
be) a single NFT representing ownership of that digital asset.

The world is becoming increasingly digital, and many artists have embraced
digital technologies to create more artistically, efficiently, or seamlessly,
depending on their chosen medium. While there will always be artists who create
for financial gain, there will also always be artists who will create out of
their creative desires and aspirations.  The digital world however does allow
for more planning to happen before a work is released, so it is very possible
that the creative process can become rigid when the end goal is
predominantly for financial gain. Also as mentioned earlier, video-game
developers are starting to develop entire games based around NFTs. There has
always been an internal war with artists and the struggle to make a living
from their creative expression, so perhaps this will not change.

## References

Abbatemarco, N., de Rossi, L. M., & Salviotti, G. (2018). The Blockchain Journey : A Guide to Practical Business Applications. Bocconi University Press. http://ebookcentral.proquest.com/lib/sae/detail.action?docID=5527935

Alman, S., & Hirsh, S. (2019). Blockchain. American Library Association. http://ebookcentral.proquest.com/lib/sae/detail.action?docID=6202109

Bailey, A. (2021, February 23). Augmented reality art: Where your home becomes the gallery. BBC News. https://www.bbc.com/news/entertainment-arts-56123958

Banafa, A. (2020). Blockchain Technology and Applications. River Publishers. http://ebookcentral.proquest.com/lib/sae/detail.action?docID=6300570

Baucherel, K. (2020). Blockchain Hurricane : The Origins, Application, and Future of Blockchain and Cryptocurrency. Business Expert Press. http://ebookcentral.proquest.com/lib/sae/detail.action?docID=6126639

Chow, A. R. (2021, March 22). What Are NFTs and Why They Are Shaking Up the Art World? Time. https://time.com/5947720/nft-art/

Cornelius, K. (2021). Betraying Blockchain: Accountability, Transparency and Document Standards for Non-Fungible Tokens (NFTs). Information, 12(9). https://doi.org/10.3390/info12090358

Helmore, E. (2021, April 30). Woman in Disaster Girl meme sells original photo as NFT for $500,000. The Guardian. https://www.theguardian.com/technology/2021/apr/30/disaster-girl-meme-nft-sale-zoe-roth

Hern, A. (2021, June 15). NFT representing Tim Berners-Lee’s source code for the web to go on sale. The Guardian. https://www.theguardian.com/artanddesign/2021/jun/15/nft-representing-tim-berners-lee-source-code-world-wide-web-sale-auction

Kugler, L. (2021). Non-Fungible Tokens and the Future of Art. Commun. ACM. https://doi.org/10.1145/3474355

LaFountain, C. (2021). Blockchain, Cryptocurrencies, and Non-Fungible Tokens: What Libraries Need to Know. Computers in Libraries, 41. https://link-gale-com.saeezproxy.idm.oclc.org/apps/doc/A661737872/EAIM?u=saeinstitute&sid=oclc&xid=bf6a59e2

Mack, O. v. (2019). Fundamentals of Smart Contracts Security. Momentum Press. http://ebookcentral.proquest.com/lib/sae/detail.action?docID=5780564

McLaughlin, R. (2021, November 6). ‘I went from having to borrow money to making $4m in a day’: how NFTs are shaking up the art world. The Guardian. https://www.theguardian.com/artanddesign/2021/nov/06/how-nfts-non-fungible-tokens-are-shaking-up-the-art-world

Pack, L. (2021, November 8). NFTs: A NEW APPROACH TO DIGITAL CONTENT. Information Today, 38. https://link-gale-com.saeezproxy.idm.oclc.org/apps/doc/A678133332/EAIM?u=saeinstitute&sid=oclc&xid=5f4886b6

Rivlin, J. (2021). Art nouveau: Are NFTs memes--or masterpieces?.(non-fungible tokens). Spectator, 347. https://southernsaelibrary.on.worldcat.org/oclc/9269306694

Sullivan, H. (2021, March 23). “Digital home” sells for $500,000 in latest NFT sale. The Guardian. https://www.theguardian.com/artanddesign/2021/mar/23/digital-home-sells-for-500000-in-latest-nft-sale

Sullivan, P. (2021, July 23). A Painting or an NFT of It: Which Will Be More Valuable? The New York Times. https://www.nytimes.com/2021/07/23/your-money/nft-art-lebron-james-damien-hirst.html

The fun in non-fungible; Decentralised finance. (2021). The Economist. https://southernsaelibrary.on.worldcat.org/oclc/9308784915

Through the looking glass; Non-fungible tokens. (2021). The Economist. https://go-gale-com.saeezproxy.idm.oclc.org/ps/i.do?p=EAIM&u=saeinstitute&id=GALE%7CA680477476&v=2.1&it=r&sid=oclc

What’s wrong with this picture? Non-fungible tokens. (2021). The Economist. https://go-gale-com.saeezproxy.idm.oclc.org/ps/i.do?p=EAIM&u=saeinstitute&id=GALE%7CA655445011&v=2.1&it=r&sid=oclc
