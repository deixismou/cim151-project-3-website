+++

title = "A Look at Historical Distruptors: Leo Fender"
comments = "false"
+++

The exact origin of the guitar is a mystery, it can be traced all the way back
to the ancient greek Kithara [ kɪˈθɑːrə ] in 2,000 BCE. It had 7 strings that
were made of either gut or sinew and is one of the two types of greek lyre, the
other being the lyra (Gorlinski, 2019)⁠.            

Around 1500 BCE the ancient Persians had a guitar-like instrument called the
Tanbur [ t̪ʰænˈbuːɾ ] which had 2-10 strings that were arranged in single, double
or triple courses (Kuiper, 2014)⁠.        

Following the Tanbur came the Spanish Vihuela [ biˈwela ] which appeared around
1,000 BCE; it had 6-7 double course strings and spread all throughout Europe
during the middle ages (Cunningham, 2014; Tyler & Sparks, 2002, p. 4)⁠.    	

Around 30 BCE in early Egypt came a stringed instrument called the Arched harp
which is seen as a relative of the guitar due to the fact that it was played by
plucking the strings (Arched Harp, 1999)⁠.

During the 1300’s came the Renaissance guitar which was very similar to the
Spanish Vihuela and had 4 double course strings (Tyler & Sparks, 2002, Chapter
1)⁠. 

In the 1600’s came the Baroque guitar which is considered as a direct ancestor
of the modern guitar, it had five courses of gut strings and movable frets made
from gut and tied around the neck (Tyler & Sparks, 2002, pp. 49–50)⁠.            

During the 1750’s changes were made to the guitar resulting in the classical
guitar, a 6th string was added and strings became cheaper to buy, during this
period the guitar had a huge decline in popularity and guitar playing composers
were few in number (Tyler & Sparks, 2002, Chapter 11)⁠.            

In the 1800’s came the 19th century guitar, it had changes made both in its
construction and the way it was played, people now used fingerstyle. This
brought the guitar closer to what we use today (Tyler & Sparks, 2002, Chapter
13)⁠. 

Probably the earliest attempt at creating an electronic instrument was the
Electric Harpsichord, developed by Jean-Baptiste Laborde in 1759. This was a
primitive keyboard instrument which used static electricity to vibrate tuned
bells. While this instrument used electricity to trigger the bell tones, the
sound generation itself was still principally acoustic (Crab, n.d.)⁠.

Although true electronic instruments began to emerge in the early 20th Century,
many of these instruments were keyboard based, and used oscillators to generate
sounds; the most famous of these are the theremin (1924) and ondes Martenot
(1928) (Holmes & Pender, 2012; Manning, 2004)⁠.

Other attempts to “electrify” instruments applied early carbon-type microphone
technology to acoustic instruments like the banjo and violin, by placing the
microphone beneath the strings or on the body of the instrument. These
instruments still worked by converting acoustic energy in much the same way that
a traditional microphone would (Roads, 1996)⁠.

The first attempt at creating an electric guitar as we know it today was by
George Beauchamp in 1931, who was trying to find a way to amplify his Hawaiian
steel guitar to be heard above an orchestra. His solution became known as the
“frying pan”, due to its all metal construction and distinctive shape (Hunter,
2013, p. 14)⁠. Beauchamp’s design utilised an electromagnetic device called a
pickup, that was mounted under the strings. Although the design of
electromagnetic pickups has improved dramatically since then, the technique is
essentially the same used in the modern pickups found in electric guitars today.
Beauchamp later became one of the principal founders of the Rickenbacker Guitar
Company (Tolinski & Di Perna, 2016, Chapter 1)⁠.

Acoustic guitars are designed to maximise the acoustic projection of the
instrument. As the strings vibrate, they transfer those vibrations through the
bridge plate to the face of the instrument known as the “soundboard”. The
soundboard is made from a thin, springy sheet of wood (usually spruce). As it
vibrates, the large surface area of the soundboard causes compressions and
rarefactions in the air surrounding it, which then resonate in the hollow body
of the instrument and are reflected outwards to the listener (Parker, 2009, pp.
159–162)⁠.

An electric guitar doesn’t require a soundboard or resonant body, it works by
converting the string vibrations into electromagnetic energy, which generates
small amounts of alternating current. This AC signal is transmitted to an
amplifier, which converts it into a large enough current to drive an audio
speaker (Parker, 2009, pp. 162–163)⁠.

The device which converts the string vibrations into electromagnetic energy is
called a “pickup”, which relies on a phenomenon called electromagnetic
induction. Like all notions in physics, electromagnetic induction is a simple
concept which can be described neatly using an impossibly complex mathematical
equation, which in this case, was devised by Michael Faraday in 1831. 

It can be stated that a magnetic flux passing through a closed-loop circuit will
induce an electromotive force upon the electrons within that circuit directly
proportional to the rate of change within the aforementioned magnetic flux. This
electromotive force will result in a current flow within the conductor which
alternates at a frequency relative to the time rate of change of magnetic flux
within the conductor (Scherz & Monk, 2016, pp. 117–120)⁠.

In other words, if you take a magnet and wrap a thin wire around it hundreds or
thousands of times, you create a device which converts the motion of the string
into tiny pulses of alternating current. Then that current is sent to an
amplifier, which turns the tiny current into a large current, which can then
push a speaker back and forth at the same frequency. 

By the late 1940s, a number of companies had established a modest industry
building electric guitars and amplifiers. Most of these companies were founded
by names which are well known to guitarists today, including Orville Gibson,
Paul Bigsby, Adolph Rickenbacker and Frederich Gretsch (Port, 2019, pp. 20–21)⁠.

All of these guitar designers were luthiers (the traditional name for a maker of
stringed instruments); although they were all innovative, they were principally
craftsmen who approached guitar building as an artisan tradition (Port, 2019, p.
98)⁠.

But one of their peers was a different breed of designer. Leo Fender was not a
luthier, nor any kind of craftsman, and had never even learned to play the
guitar (Port, 2019, p. 31)⁠. He was a self-taught electrical engineer, who began
his career as a radio repair shop owner, but through his love of audio, had come
to design and build amplifiers and PA systems. Fender thought like an engineer,
he admired function and efficiency, cared nothing for tradition, and clearly
understood the value of “manufacturability” (Port, 2019, Chapter 2)⁠.

Inspired by early steel guitars, Fender saw the potential for a radically new
fretted guitar design built entirely from solid wood. Fender believed that a
solid-body design would be less affected by feedback, and could produce a
sharper and clearer sound than hollow-body instruments (Hunter, 2013, p. 18)⁠.

By 1951, the Telecaster and Precision Bass had arrived on the market. Both
instruments were solid-body constructions made from ash and maple, included a
modular design with detachable “bolt-on” necks, had adjustable bridge saddles,
and a new pickup design with variable tone and volume controls (Owens, n.d.)⁠.

Other companies quickly followed. In 1952, Gibson (Fender’s larger competitor)
released it’s own solid-body design: the Gibson Les Paul (named after the
country music star and personal friend of Leo Fender). Still a fledgling
company, Fender needed to respond quickly to maintain its newly established
market share (Port, 2019, Chapter 13,16)⁠. The answer came in 1954, with a
guitar design that was so innovative, it needed a name worthy of the space-race
era. Fender called it the Stratocaster.

The Stratocaster featured all of the advantages of the Telecaster, but included
a body-contoured shape, three single-coil pickups with tone-blending, a
spring-action vibrato system and a host of other functional and ergonomic
features. The Stratocaster also extended Fender’s approach to modular design by
mounting all electronic components on the scratch plate of the instrument, which
made installation and assembly more efficient (Hunter, 2013, pt. 2)⁠. 

Fender had taken all of the feedback and input he received from professional
musician’s an incorporated these aesthetic and practical elements into a design
which he believed would be the ultimate instrument of its kind. 

Fender initially marketed its instruments to guitarists within the country and
western genres, which were favourites of Leo Fender himself, but they have since
been adopted by musicians of almost every genre of popular music (Tolinski & Di
Perna, 2016, pp. 169–170)⁠. The Stratocaster itself has become the staple
instrument of guitar icons like Eric Clapton, Jimi Hendrix, David Gilmour, and
countless others. These icons, and the sounds they were able to create has in
turn influenced generations of music listeners and helped to define the eras in
which they wrote and performed.

70 years later, these features are so ubiquitous that we often take them for
granted, but Fender was the first manufacturer to produce instruments which
were not only exceptionally rugged and reliable, but highly configurable,
versatile, easy to repair, and could be mass produced at a retail price of
$189.50 (equivalent to about $1,800 today), almost half the price of
competitors (Port, 2019, p. 115)⁠. The undeniable success of Fender’s
solid-body designs was impossible for its competitors to ignore, and have
prompted a host of other iconic instruments of their kind, such as the Gibson
Les Paul (Port, 2019, Chapter 13)⁠.

For professional musicians, this was a game-changing leap forward; Leo Fender
had done for the guitar what Henry Ford had done for the Automobile; he created
an affordable tool with near limitless creative potential, which is still as
relevant today as it was in 1954.

## References

Arched harp. (1999). Encyclopedia Britannica. https://www.britannica.com/art/arched-harp

Crab, S. (n.d.). 120 Years of Electronic Music – The history of electronic musical instruments from 1800 to 2019. Retrieved October 10, 2021, from https://120years.net/wordpress/

Cunningham, J. M. (Ed.). (2014). Vihuela. In Encyclopedia Britannica. https://www.britannica.com/art/vihuela

Gorlinski, V. (Ed.). (2019). Kithara. In Encyclopedia Britannica. https://www.britannica.com/art/kithara

Holmes, T., & Pender, T. M. (2012). Electronic and experimental music : technology, music, and culture (4th ed.). Routledge. http://www.routledge.com/cw/holmes

Hunter, D. (2013). The Fender Stratocaster. Voyageur Press.

Kuiper, K. (Ed.). (2014). Ṭanbūr. In Encyclopedia Britannica. https://www.britannica.com/art/tanbur

Manning, P. D. (2004). Electronic and Computer Music. Oxford University Press. http://ebookcentral.proquest.com/lib/sae/detail.action?docID=3051899

Owens, J. (n.d.). The One That Started It All: A Telecaster History. Fender.Com. Retrieved October 10, 2021, from https://www.fender.com/articles/gear/the-one-that-started-it-all-a-telecaster-history

Parker, B. (2009). Good Vibrations : The Physics of Music. Johns Hopkins University Press. http://ebookcentral.proquest.com/lib/sae/detail.action?docID=3318437

Port, I. S. (2019). The Birth Of Loud. Scribner. https://www.simonandschuster.com/books/The-Birth-of-Loud/Ian-S-Port/9781501141737

Roads, C. (1996). Early Electronic Music Instruments: Time Line 1899-1950. Computer Music Journal, 20(3), 20–23. https://doi.org/10.2307/3680817

Scherz, P., & Monk, S. (2016). Practical Electronics for Inventors (4th ed.). McGraw-Hill Education. https://www.mheducation.com/highered/product/practical-electronics-inventors-fourth-edition-scherz-monk/9781259587542.html

Tolinski, B., & Di Perna, A. (2016). Play it loud : an epic history of the style, sound, and revolution of the electric guitar. Doubleday.

Tyler, J., & Sparks, P. (2002). The Guitar and Its Music : From the Renaissance to the Classical Era. Oxford University Press. http://ebookcentral.proquest.com/lib/sae/detail.action?docID=422842


