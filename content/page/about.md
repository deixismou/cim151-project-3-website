---
title: About Us
image: placholder1.jpeg
type: page
comments: false
---
## Who are we? What do we do?

Jape Digital Art Consulting began as an assessment excersise for the SAE course
_CIM151: Contemporary Industrial Practices_, and to be perfectly honest, did not
progress any further than that; it is a hypothetical company which was devised
as a way to explore the potential for new products and services in the digital
art marketplace.

Specifically, we have imagined an education and digital resources platform for
artists who require support and guidance for selling and marketing their artwork
in the dauntingly technical world of NFTs and crypto art.

## Meet our fantastic team

{{< rawhtml >}}
<div class='box'>
<div class='row'>
<div class='col-4'>
<img src="/images/mackenzie.png" width="100%"/>
</div>
<div class='col-8'>
{{</ rawhtml >}}
### Mackenzie Tremain 
Mackenzie and is one of the co-founders for this company and specializes in
marketing, and Design. She has been doing Graphic Design for exactly 8 months
and is absolutely not qualified for this position but alas here she is! Her
favourite food is Pizza (Dominos is the best!) Check out her portfolio to see
some of her work and learn more about her.

{{<button-group>}}
{{<button title="See Portfolio" link="https://maeyeo2868.myportfolio.com">}}
{{</button-group>}}


{{< rawhtml >}}
</div>
</div>
</div>
{{</ rawhtml >}}


{{< rawhtml >}}
<div class='box'>
<div class='row'>
<div class='col-4'>
<img src="/images/brodie.png" width="100%"/>
</div>
<div class='col-8'>
{{</ rawhtml >}}
### Brodie Halford 
Brodie comes from a creative arts background, with her study of graphic design
allowing her to work with teams to brainstorm and whiteboard concepts. She cares
about the little details and uses her skills to determine features, user flows
and style guides to unify the design process.  She has a love for digital
collages, creating fantasy worlds she dreams she will one day stumble upon in
real life. On her days off, you can find her taking quick dips in the ocean and
listening to David Bowie tunes on repeat.

{{<button-group>}}
{{<button title="See Portfolio" link="https://brodiehalford1.myportfolio.com">}}
{{</button-group>}}


{{< rawhtml >}}
</div>
</div>
</div>
{{</ rawhtml >}}

{{< rawhtml >}}
<div class='box'>
<div class='row'>
<div class='col-4'>
<img src="/images/corey.png" width="100%"/>
</div>
<div class='col-8'>
{{</ rawhtml >}}
### Corey Bresnan 
Corey is the Chief Technical Officer at JAPE. He is a musician and performer
who has worked in live sound production and mutimedia. He's also a skilled web
developer and programmer, and his professional work has ventured into the realms
of industrial design and electronics. Corey has studied Linguistics at ANU, and
is currently undertaking a Bachelor of Audio at SAE in the hope of building a
career in studio production and audio engineering. 

{{<button-group>}}
{{<button title="See Portfolio" link="https://deixismou.gitlab.io/sae-personal-portfolio/">}}
{{</button-group>}}

{{< rawhtml >}}
</div>
</div>
</div>
{{</ rawhtml >}}

## Are you still not convinced?

Why don't you book an appointment to learn more about our unique services
and how we can tailor our courses to suit your needs? After all, you are as
unique as the art you create! But we're all about that here.

{{<button-group>}}
{{<button title="Contact Us" link="/page/contact/">}}
{{</button-group>}}
