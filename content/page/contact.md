---
title: We're Only Kidding!
comments: false
---

JAPE Consulting isn't a real company, we just made it up because Miguel thought
it was a good idea. But you can fill out this form as many times as you like...
it doesn't actually do anything.


{{<form-contact>}}
