![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---
A static website built from Hugo. Created for SAE CIM151 course.

This site can be viewed at: https://deixismou.gitlab.io/cim151-project-3-website/

---

Recently I've been considering a method for creating highly customised websites
using the Hugo static site generator and GitLab Pages platform. For a long time,
I've been of the opinion that static site generators are an ideal way to build
modern websites, so I was glad I could use this project to experiment with the
toolchain.

Static sites have the following benefits:

* Low overhead and hosting resources required, since page generation is pre-processed and the server requires few moving parts. There is no database, no code execution on the server-side, and since they are fast and reliable, there is little need for load-balancing and other infrastructure for even  moderate-traffic sites (which can get expensive quickly).
* Since there is no code execution on the server-side, there are fewer security vulnerabilities and the hosting server can be hardened more easily.
* Most of the interactivity is processed on the client, meaning fewer dependencies on the server-side. This makes statics sites highly portable. You can host them from almost anywhere.
* The websites submitted for this assignment were built using Hugo, and hosted in a repository on GitLab. Each time the site is updated and the code is pushed to the repository, GitLab pre-generates every possible permutation of every page on the site, and publishes the generated pages to GitLab's free static hosting service. This whole process takes less than 30 seconds to execute from code-change to published result! Which is pretty neat.
* Another benefit is that the site content is managed within a Git repository, so the entire history of the site, from the first change to the last is recorded in one place. In this sense, Git is working like track-changes on steroids.
* There are a few additional things I'd set up for a production website, but were way beyond the scope of this project. For example, using Git's large file storage mechanism for managing binary assets, and getting more value out of Hugo's build pipeline for pre-processing and minifying elements of the code-base.
* Additionally, for this project, I wasn't prepared to design a site completely from scratch, so I found a usable template and adapted it. Were I building an actual company website or a professional portfolio which I planned to publish, I would design my own theme from scratch with the features I wanted it to have. I have given some thought to this in the past but it seemed like overkill for this submission.
